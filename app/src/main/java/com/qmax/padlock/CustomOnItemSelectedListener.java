package com.qmax.padlock;

import android.view.View;
import android.widget.AdapterView;
import android.widget.Toast;

/**
 * Created by Veera on 16-10-2017.
 */

class CustomOnItemSelectedListener implements AdapterView.OnItemClickListener {
    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        Toast.makeText(adapterView.getContext(),
                "OnItemSelectedListener : " + adapterView.getItemAtPosition(i).toString(),
                Toast.LENGTH_SHORT).show();

    }
}
