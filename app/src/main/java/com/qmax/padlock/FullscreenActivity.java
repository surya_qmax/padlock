package com.qmax.padlock;

import android.app.Activity;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.qmax.padlock.util.SystemUiHider;

import java.util.ArrayList;
import java.util.List;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 *
 * @see SystemUiHider
 */
public class FullscreenActivity extends Activity {
    private static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 1;
    Toast toast;
    Button Unlock;
    Spinner mac;
    TextView M1,M2,M3;

    /**
     * Whether or not the system UI should be auto-hidden after
     * {@link #AUTO_HIDE_DELAY_MILLIS} milliseconds.
     */
    private static final boolean AUTO_HIDE = true;

    /**
     * If {@link #AUTO_HIDE} is set, the number of milliseconds to wait after
     * user interaction before hiding the system UI.
     */
    private static final int AUTO_HIDE_DELAY_MILLIS = 3000;

    /**
     * If set, will toggle the system UI visibility upon interaction. Otherwise,
     * will show the system UI visibility upon interaction.
     */
    private static final boolean TOGGLE_ON_CLICK = true;

    /**
     * The flags to pass to {@link SystemUiHider#getInstance}.
     */
    private static final int HIDER_FLAGS = SystemUiHider.FLAG_HIDE_NAVIGATION;

    /**
     * The instance of the {@link SystemUiHider} for this activity.
     */
//    private SystemUiHider mSystemUiHider;


    private static final int REQUEST_ENABLE_BT = 1;
    // Stops scanning after 10 seconds.
    private static final long SCAN_PERIOD = 5000;
    public static String FIND_CLASS = "find_class";
    private BluetoothAdapter mBluetoothAdapter;
    private boolean mScanning;
    private Handler mHandler;
    //    private String mDeviceAddress = "7C:EC:79:71:60:65";
    public String mDeviceAddress;
    //    private String mDeviceAddress = "7C:EC:79:71:55:89";
    public BluetoothLeService mBluetoothLeService;
    private ProgressBar progressBar = null;
    public TextView connection_status = null;

    public ImageView imgview;
    // Device scan callback.
    private BluetoothAdapter.LeScanCallback mLeScanCallback =
            new BluetoothAdapter.LeScanCallback() {

                @Override
                public void onLeScan(final BluetoothDevice device, int rssi, byte[] scanRecord) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Log.v("**PADLOCK**", "  ** Device Address : " + device.getAddress().toString());
                            if (device.getAddress().toString().equals(mDeviceAddress)) {
                                //connect devices
                                Log.v("**PADLOCK1**", "  ** Device Address : " + device.getAddress().toString());
                                if (mBluetoothLeService != null) {
                                    final boolean result = mBluetoothLeService.connect(mDeviceAddress);
                                    Log.d("**LOCK", "Connect request resul t=" + result);
                                    progressBar.setVisibility(View.INVISIBLE);
                                }
                            }
                        }
                    });
                }
            };


    private final ServiceConnection mServiceConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName componentName, IBinder service) {
            mBluetoothLeService = ((BluetoothLeService.LocalBinder) service).getService();
            if (!mBluetoothLeService.initialize()) {
                Log.e("**LOCK**", "Unable to initialize Bluetooth");
                finish();
            }
            // Automatically connects to the device upon successful start-up initialization.
            mBluetoothLeService.connect(mDeviceAddress);
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            mBluetoothLeService = null;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fullscreen);
        connection_status = findViewById(R.id.connection_state);
        progressBar = findViewById(R.id.loading_spinner);
        Unlock = findViewById(R.id.unlock_button);
        mac = findViewById(R.id.macaddr);

        Unlock.setVisibility(View.INVISIBLE);
        checkAndRequestPermissions();

        LayoutInflater inflater = getLayoutInflater();
        View alertLayout = inflater.inflate(R.layout.deviceselect, null);

        M1=alertLayout.findViewById(R.id.mac1);
        M2=alertLayout.findViewById(R.id.mac2);
        M3=alertLayout.findViewById(R.id.mac3);

//        TextView txt1=alertLayout.findViewById(R.id.mac1);
//
//        txt1.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Log.d("FullscreenActivity","clicked:::");
//            }
//        });

        List<String> list = new ArrayList<String>();
        list.add("E7:F0:C5:14:FF:D5");
      //  list.add("7C:EC:79:71:60:65");
      //  list.add("7C:EC:79:71:55:89");
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mac.setAdapter(dataAdapter);

      mac.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
          @Override
          public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
              mDeviceAddress=adapterView.getItemAtPosition(i).toString();
              Log.d("Selected Mac id","::"+mDeviceAddress);
              scanLeDevice(true);
//              mBluetoothLeService.connect(mDeviceAddress);
          }

          @Override
          public void onNothingSelected(AdapterView<?> adapterView) {

          }
      });

        final AlertDialog.Builder alert=new AlertDialog.Builder(this);
        alert.setView(alertLayout);
        alert.setCancelable(true);



//        alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
//
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
////                Toast.makeText(getBaseContext(), "Cancel clicked", Toast.LENGTH_SHORT).show();
//                CloseApplication1();
//            }
//        });
//
//        alert.setPositiveButton("Connect", new DialogInterface.OnClickListener() {
//
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                scanLeDevice(true);
//            }
//        });


        final AlertDialog dialog = alert.create();
        dialog.show();


        M1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                mDeviceAddress=M1.getText().toString();
                mDeviceAddress="7C:EC:79:71:71:B7";
                Log.d("Selected","::"+mDeviceAddress);
                scanLeDevice(true);
                dialog.dismiss();
            }
        });
      /*  M2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                mDeviceAddress=M2.getText().toString();
                mDeviceAddress="7C:EC:79:71:60:65";
                Log.d("Selected","::"+mDeviceAddress);
                scanLeDevice(true);
                dialog.dismiss();
            }
        });
        M3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                mDeviceAddress=M3.getText().toString();
                mDeviceAddress="7C:EC:79:71:55:89";
                Log.d("Selected","::"+mDeviceAddress);
                scanLeDevice(true);
                dialog.dismiss();
            }
        });*/



        Unlock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (AUTO_HIDE) {
                    if (mConnected) {
                        if (mBluetoothLeService != null) {
                            byte[] BYTE_SEND_RTC_TIME = {(byte) 0xfe, (byte) 0xaf, 0x01, 0x01, 0x02,
                                    0x02, 0x20, 0x20, 0x20, 0x20,
                                    0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20,
                                    0x20, 0x41, 0x54};
                            try {
                                final Toast toast = Toast.makeText(getApplicationContext(), "Opening Lock", Toast.LENGTH_SHORT);
                                toast.show();
                                Handler handler = new Handler();
                                handler.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        toast.cancel();
                                    }
                                }, 800);
                                mBluetoothLeService.writeCustomCharacteristic(BYTE_SEND_RTC_TIME);
                                Log.v("**LOCK**", "Write function 1122... success");
                                LockApplication.FLAG_REQUEST_DEVICE_STATUS = 0;
                                StringBuilder sb = new StringBuilder();
                                for (byte b : BYTE_SEND_RTC_TIME) {
                                    if (sb.length() > 0) {
                                        sb.append(':');
                                    }
                                    sb.append(String.format("%02x", b));
                                }
                            } catch (Exception e) {
                                Log.v("**LOCK**", "UUUUU Write function unlock command... failed");
                            }
                        }

                    }
                }
            }
        });


        // Upon interacting with UI controls, delay any scheduled hide()
        // operations to prevent the jarring behavior of controls going away
        // while interacting with the UI.
//        findViewById(R.id.unlock_button).setOnTouchListener(mDelayHideTouchListener);

        Intent gattServiceIntent = new Intent(this, BluetoothLeService.class);
        bindService(gattServiceIntent, mServiceConnection, BIND_AUTO_CREATE);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        // Trigger the initial hide() shortly after the activity has been
        // created, to briefly hint to the user that UI controls
        // are available.
        mHandler = new Handler();
        if (!getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)) {
            Toast.makeText(this, R.string.ble_not_supported, Toast.LENGTH_SHORT).show();
            finish();
        }

        // Initializes a Bluetooth adapter.  For API level 18 and above, get a reference to
        // BluetoothAdapter through BluetoothManager.
        final BluetoothManager bluetoothManager =
                (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
        mBluetoothAdapter = bluetoothManager.getAdapter();

        if (mBluetoothAdapter == null) {
            Toast.makeText(this, R.string.error_bluetooth_not_supported, Toast.LENGTH_SHORT).show();
            finish();
            return;
        }
    }


    private boolean checkAndRequestPermissions() {

        int loc = ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION);
        int loc1 = ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION);
        List<String> listPermissionsNeeded = new ArrayList<>();

        if (loc != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(android.Manifest.permission.ACCESS_COARSE_LOCATION);

        }
        if (loc1 != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(android.Manifest.permission.ACCESS_FINE_LOCATION);
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray
                    (new String[listPermissionsNeeded.size()]), REQUEST_ID_MULTIPLE_PERMISSIONS);
            return false;
        }
        return true;
    }

    private void scanLeDevice(final boolean enable) {
        if (enable) {
            progressBar.setVisibility(View.VISIBLE);
            connection_status.setText("Device Scanning");

            // Stops scanning after a pre-defined scan period.
            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mScanning = false;
                    mBluetoothAdapter.stopLeScan(mLeScanCallback);
                    invalidateOptionsMenu();
                }
            }, SCAN_PERIOD);

            mScanning = true;
            mBluetoothAdapter.startLeScan(mLeScanCallback);
        } else {
            mScanning = false;
            mBluetoothAdapter.stopLeScan(mLeScanCallback);
            progressBar.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (!mBluetoothAdapter.isEnabled()) {
            if (!mBluetoothAdapter.isEnabled()) {
                Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
            }
        }

        scanLeDevice(true);
        registerReceiver(mGattUpdateReceiver, makeGattUpdateIntentFilter());
//        CloseApplication1();
    }


    private ArrayList<ArrayList<BluetoothGattCharacteristic>> mGattCharacteristics =
            new ArrayList<ArrayList<BluetoothGattCharacteristic>>();
    private boolean mConnected = false;
    // Handles various events fired by the Service.
    // ACTION_GATT_CONNECTED: connected to a GATT server.
    // ACTION_GATT_DISCONNECTED: disconnected from a GATT server.
    // ACTION_GATT_SERVICES_DISCOVERED: discovered GATT services.
    // ACTION_DATA_AVAILABLE: received data from the device.  This can be a result of read
    //                        or notification operations.
    public final BroadcastReceiver mGattUpdateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();
            if (BluetoothLeService.ACTION_GATT_CONNECTED.equals(action)) {
                mConnected = true;
                connection_status.setText("Device Connected");
                progressBar.setVisibility(View.INVISIBLE);
                Unlock.setVisibility(View.VISIBLE);
                final Toast toast = Toast.makeText(getApplicationContext(), "Device Connected", Toast.LENGTH_SHORT);
                toast.show();
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        toast.cancel();
                    }
                }, 800);
                invalidateOptionsMenu();
                Log.v("**LOCK**", "DEVICE CONNECTED : " + BluetoothLeService.ACTION_GATT_CONNECTED.equals(action));
            } else if (BluetoothLeService.ACTION_GATT_DISCONNECTED.equals(action)) {
                mConnected = false;
                progressBar.setVisibility(View.INVISIBLE);
                connection_status.setText("Device DisConnected");
                final Toast toast = Toast.makeText(getApplicationContext(), "Device DisConnected", Toast.LENGTH_SHORT);
                toast.show();
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        toast.cancel();
                    }
                }, 800);
                invalidateOptionsMenu();
                Log.v("**LOCK**", " DEVICE DISCONNECTED : " + BluetoothLeService.ACTION_GATT_DISCONNECTED.equals(action));
                CloseApplication();
            } else if (BluetoothLeService.ACTION_GATT_SERVICES_DISCOVERED.equals(action)) {
                // Show all the supported services and characteristics on the user interface.
                //displayGattServices(mBluetoothLeService.getSupportedGattServices());
            } else if (BluetoothLeService.ACTION_DATA_AVAILABLE.equals(action)) {
//                displayData(intent.getStringExtra(BluetoothLeService.EXTRA_DATA));
                Log.v("**LOCK**", " RECEIVING BYTE : " + intent.getStringExtra(BluetoothLeService.EXTRA_DATA));
            }
        }
    };
    private BluetoothGattCharacteristic mNotifyCharacteristic;

    private static IntentFilter makeGattUpdateIntentFilter() {
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(BluetoothLeService.ACTION_GATT_CONNECTED);
        intentFilter.addAction(BluetoothLeService.ACTION_GATT_DISCONNECTED);
        intentFilter.addAction(BluetoothLeService.ACTION_GATT_SERVICES_DISCOVERED);
        intentFilter.addAction(BluetoothLeService.ACTION_DATA_AVAILABLE);
        intentFilter.addAction(BluetoothLeService.EXTRA_DATA);
        return intentFilter;
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(mGattUpdateReceiver);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbindService(mServiceConnection);
        scanLeDevice(false);
        mBluetoothLeService = null;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.gatt_services, menu);
        if (mConnected) {
            menu.findItem(R.id.menu_connect).setVisible(false);
            menu.findItem(R.id.menu_disconnect).setVisible(true);
        } else {
            menu.findItem(R.id.menu_connect).setVisible(true);
            menu.findItem(R.id.menu_disconnect).setVisible(false);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_connect:
                mBluetoothLeService.connect(mDeviceAddress);
                return true;
            case R.id.menu_disconnect:
                mBluetoothLeService.disconnect();
                progressBar.setVisibility(View.INVISIBLE);
                CloseApplication();
                return true;
            case R.id.menu_scan:
                progressBar.setVisibility(View.VISIBLE);
//                mBluetoothLeService.disconnect();
                connection_status.setText("Device Scanning");
                scanLeDevice(true);
                return true;
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void CloseApplication() {
        new android.os.Handler().postDelayed(
                new Runnable() {
                    public void run() {

                        try {
                            Log.d("Application", "Closed");
                            System.exit(0);

                        } catch (Exception e) {

                            e.printStackTrace();
                        }
                    }
                }, 3000);

    }

    public void CloseApplication1() {
        new android.os.Handler().postDelayed(
                new Runnable() {
                    public void run() {

                        try {
                            Log.d("Application", "Closed");
                            final Toast toast = Toast.makeText(getApplicationContext(), "Sorry!! Cannot Find Lock!!", Toast.LENGTH_SHORT);
                            toast.show();
                            Handler handler = new Handler();
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    toast.cancel();
                                }
                            }, 800);
                            System.exit(0);

                        } catch (Exception e) {

                            e.printStackTrace();
                        }
                    }
                }, 3000);

    }
}
